using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnController : AllGameObjects
{
    public GameObject weakEnemy; // spawning Enemies
	public GameObject middleEnemy;
	public GameObject strongEnemy;
	public GameObject boosterDestroyStrongs;

	public GameObject spawnPoint; // Center of screen
	public float timeBetweenSpawn; // it needs get less
	
	public Text pointsText;

	private int randX;
	private int randY;
	private Vector2 spawnPosition;

	private readonly int idWeakEnemy = 0;
	private readonly int idMiddleEnemy = 1;
	private readonly int idStrongEnemy = 2;

	private Vector2 spawnPosBooster;

	private void Start()
	{
		StartCoroutine(SpawnProcess());
		timeBetweenSpawn = 2;
	}

	IEnumerator SpawnProcess()
	{
		yield return new WaitForSeconds(timeBetweenSpawn);
		ChooseSpawnPosition();
		spawnPosition = new Vector2(randX, randY);
		
		ChooseEnemyToSpawn();
		ChooseSpawnTime();

		if (CheckIsGameLoss())
		{
			//SavePlayerRecord();
			string newRecord = pointsText.text;
			PlayerPrefs.SetString("playerRecord", newRecord);
			SceneManager.LoadScene(3); // Achievements scene
		}

		if (CheckTimeToSpawnBooster2() == true && boosters2.Length <= 1)
		{
			ChooseBoosterToSpawn();
		}

		Repeat();
	}

	private void Repeat()
	{
		StartCoroutine(SpawnProcess());
	}

	private readonly bool[] gameObjectPositionsX = new bool[5];
	private readonly bool[] gameObjectPositionsY = new bool[5];
	
	private void ChooseSpawnPosition()
	{
		strongEnemies = GameObject.FindGameObjectsWithTag("Strong Enemy");
		middleEnemies = GameObject.FindGameObjectsWithTag("Middle Enemy");
		weakEnemies = GameObject.FindGameObjectsWithTag("Weak Enemy");
		boosters2 = GameObject.FindGameObjectsWithTag("Booster 2");

		float xPos, yPos;
		for (int i = strongEnemies.Length - 1; i > 0; --i) // make sign in matrix to find free room
		{
			xPos = strongEnemies[i].GetComponent<Transform>().position.x;
			gameObjectPositionsX[ConvertForPosition((int)xPos)] = true;

			yPos = strongEnemies[i].GetComponent<Transform>().position.y;
			gameObjectPositionsY[ConvertForPosition((int)yPos)] = true;
		}

		for (int i = middleEnemies.Length - 1; i > 0; --i) // make sign in matrix to find free room
		{
			xPos = middleEnemies[i].GetComponent<Transform>().position.x;
			gameObjectPositionsX[ConvertForPosition((int)xPos)] = true;

			yPos = middleEnemies[i].GetComponent<Transform>().position.y;
			gameObjectPositionsY[ConvertForPosition((int)yPos)] = true;
		}

		for (int i = weakEnemies.Length - 1; i > 0; --i) // make sign in matrix to find free room
		{
			xPos = weakEnemies[i].GetComponent<Transform>().position.x;
			gameObjectPositionsX[ConvertForPosition((int)xPos)] = true;

			yPos = weakEnemies[i].GetComponent<Transform>().position.y;
			gameObjectPositionsY[ConvertForPosition((int)yPos)] = true;
		}

		for (int i = boosters2.Length - 1; i > 0; --i) // make sign in matrix to find free room
		{
			xPos = boosters2[i].GetComponent<Transform>().position.x;
			gameObjectPositionsX[ConvertForPosition((int)xPos)] = true;

			yPos = boosters2[i].GetComponent<Transform>().position.y;
			gameObjectPositionsY[ConvertForPosition((int)yPos)] = true;
		}

		randX = Random.Range(-2, 2); // if this room has already placed but we have other free room
		randY = Random.Range(-2, 2);
		if (gameObjectPositionsX[ConvertForPosition(randX)] == true && SetHasFreeCoordinateX())
		{
			int startOrderingNumber = 1;
			for (int i = 0; i < 5; i++)
			{
				if (gameObjectPositionsX[ConvertForPosition(randX)] != false)
				{
					randX = startOrderingNumber;
				}
				else
				{
					break;
				}

				startOrderingNumber *= -1;
				if (i == 2 || i == 4)
				{
					startOrderingNumber--;
				}
			}
		}
		else // we don't have other free room
		{
			if (gameObjectPositionsY[ConvertForPosition(randY)] == true)
			{
				int startOrderingNumber = 2;
				for (int i = 0; i < 5; i++)
				{
					if (gameObjectPositionsY[ConvertForPosition(randY)] != false)
					{
						randY = startOrderingNumber;
					}
					else
					{
						break;
					}

					startOrderingNumber *= -1;
					if (i == 2 || i == 4)
					{
						startOrderingNumber--;
					}
				}
			}
		}

		for (int i = 0; i < 5; i++)
		{
			gameObjectPositionsX[i] = false;
			gameObjectPositionsY[i] = false;
		}
	}

	private int ConvertForPosition(int number)
	{
		switch (number)
		{
			case -2: return 0;
			case -1: return 1;
			case 0: return 2;
			case 1: return 3;
			case 2: return 4;
			default:
				break;
		}

		return 2;
	}

	protected bool SetHasFreeCoordinateX()
	{
		int num = 0;
		for (int i = 0; i < 5; ++i)
		{
			if (gameObjectPositionsX[i] == true)
			{
				num++;
			}
		}

		return num != 5;
	}

	private void ChooseEnemyToSpawn()
	{
		// (object to spawn, spawn place, without rotation)
		int idRandEnemy = ExpandEnemyRange();
		switch (idRandEnemy)
		{
			case 0:
				Instantiate(weakEnemy, spawnPosition, Quaternion.identity); break;
			case 1:
				Instantiate(middleEnemy, spawnPosition, Quaternion.identity); break;
			case 2:
				Instantiate(strongEnemy, spawnPosition, Quaternion.identity); break;
		}
	}

	private void ChooseBoosterToSpawn()
	{
		ChooseSpawnPosition();
		spawnPosBooster = new Vector2(randX, randY);
		Instantiate(boosterDestroyStrongs, spawnPosBooster, Quaternion.identity);
	}

	private bool CheckTimeToSpawnBooster2()
	{
		int randNumber = Random.Range(0, 9);
		return (randNumber == 3);
	}

	private void ChooseSpawnTime()
	{
		timeBetweenSpawn = Random.Range(1, 2);
	}

	private int ExpandEnemyRange()
	{
		int result = 0;
		int score = int.Parse(pointsText.text);
		if (score <= 40)
		{
			result = Random.Range(idWeakEnemy, idWeakEnemy + 1);
		}
		else if(score <= 100 && score > 40)
		{
			result = Random.Range(idWeakEnemy, idMiddleEnemy + 1);
		}
		else if (score > 100)
		{
			result = Random.Range(idWeakEnemy, idStrongEnemy + 1);
		}

		return result;
	}

	private bool CheckIsGameLoss()
	{
		int enemyNumberOnScreen =
				GameObject.FindGameObjectsWithTag("Weak Enemy").Length +
				GameObject.FindGameObjectsWithTag("Middle Enemy").Length +
				GameObject.FindGameObjectsWithTag("Strong Enemy").Length;

		if (enemyNumberOnScreen < 13) // we always have +3 enemies on screen
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}