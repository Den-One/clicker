using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;
using System;

public class Enemy : MonoBehaviour
{
    private bool isAlive;

    private AudioSource hitSound;
    public AudioClip audioClip;
    
    public int Health; // it depends on enemy type

    private int weakEnemyHealthValue = 1;
    private int middleEnemyHealthValue = 2;
    private int strongEnemyHealthValue = 3;

    [SerializeField] int points;
    public Text pointsText;

    public void OnMouseDown()
	{
        Health--;
        hitSound.PlayOneShot(audioClip);
        if (Health <= 0 && isAlive)
        {
            EnemyDies();
        }
    }

    private void EnemyDies()
	{
        isAlive = false;
        points++;
        pointsText.text = setNewValue();
        Destroy(gameObject, 0.3f);
	}

    protected string setNewValue()
    {
        int number = Convert.ToInt32(pointsText.text);
        number += points;
        string result = Convert.ToString(number);
        return result;
    }

    void Start()
    {
        isAlive = true;
        SetHealth();
        hitSound = gameObject.GetComponent<AudioSource>();
    }
        
    void SetHealth()
    {
        string enemyLevel = gameObject.tag;

        if (ConvertToInt(pointsText.text) / 100 > 0)
        {
            weakEnemyHealthValue += ConvertToInt(pointsText.text) / 100;
            middleEnemyHealthValue += ConvertToInt(pointsText.text) / 100;
            strongEnemyHealthValue += ConvertToInt(pointsText.text) / 100;
        }

        switch (enemyLevel)
        {
             case "Weak Enemy":
                Health = weakEnemyHealthValue; break;
             case "Middle Enemy":
                Health = middleEnemyHealthValue; break;
             case "Strong Enemy":
                Health = strongEnemyHealthValue; break;
        }
    }

    private int ConvertToInt(string text)
	{
        int result = Convert.ToInt32(text);
        return result;
	}
}