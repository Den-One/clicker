using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : SceneIndexs
{
    public void ExitFromGame()
	{
        Application.Quit();
	}

    public void GoToAchievements()
	{
		SceneManager.LoadScene(idAchievementsScene);
	}

    public void GoToTitles()
	{
		SceneManager.LoadScene(idTitlesScene);
	}

	public void GoToRules()
	{
		SceneManager.LoadScene(idRulesScene);
	}
}
