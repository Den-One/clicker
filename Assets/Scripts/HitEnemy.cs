using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEnemy : MonoBehaviour
{
	private Animator animator;
	private void Start()
	{
		animator = GetComponent<Animator>();
	}

	private void OnMouseDown()
	{
		Debug.Log("OnMouseDown");
		animator.SetTrigger("Hit");
	}
}
