using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneIndexs : MonoBehaviour
{
	protected int idMainMenu = 0;
	protected int idNewGameScene = 1;
	protected int idTitlesScene = 2;
	protected int idAchievementsScene = 3;
	protected int idRulesScene = 4;
}
