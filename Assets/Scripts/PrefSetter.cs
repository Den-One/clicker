using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine;

public class PrefSetter : MonoBehaviour
{
	public Text record_1;
	public Text record_2;
	public Text record_3;

	private void Start()
	{
		setOldRecords();
		setNewRecord();
	}
	public void setNewRecord()
	{
		if (PlayerPrefs.HasKey("playerRecord"))
		{
			string recordFromSave = PlayerPrefs.GetString("playerRecord");
			Debug.Log(recordFromSave);
			ChooseRecordPlaceFor(recordFromSave);
		}

		PlayerPrefs.SetString("playerRecord", "0");
		saveRecords();
	}

	private void saveRecords()
	{
		PlayerPrefs.SetString("record1", record_1.text);
		PlayerPrefs.SetString("record2", record_2.text);
		PlayerPrefs.SetString("record3", record_3.text);
	}

	private void setOldRecords()
	{
		if (PlayerPrefs.HasKey("record1"))
		{
			record_1.text = PlayerPrefs.GetString("record1");
			record_2.text = PlayerPrefs.GetString("record2");
			record_3.text = PlayerPrefs.GetString("record3");
		}
	}

	private void ChooseRecordPlaceFor(string recordFromSave)
	{
		int intRecord_1 = Convert.ToInt32(record_1.text);
		int intRecord_2 = Convert.ToInt32(record_2.text);
		int intRecord_3 = Convert.ToInt32(record_3.text);
		int intRecFromSave = Convert.ToInt32(recordFromSave);

		if (intRecFromSave > intRecord_3 && intRecFromSave <= intRecord_2)
		{
			record_3.text = recordFromSave;
		}
		else if (intRecFromSave > intRecord_2 && intRecFromSave <= intRecord_1)
		{
			record_3.text = record_2.text;
			record_2.text = recordFromSave;
		}
		else if (intRecFromSave > intRecord_1)
		{
			record_3.text = record_2.text;
			record_2.text = record_1.text;
			record_1.text = recordFromSave;
		}
	}

	public void ResetResults()
	{
		record_1.text = "0";
		PlayerPrefs.SetString("record1", record_1.text);

		record_2.text = "0";
		PlayerPrefs.SetString("record2", record_2.text);

		record_3.text = "0";
		PlayerPrefs.SetString("record3", record_3.text);
	}
}