using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEffects : MonoBehaviour
{
    private RectTransform effectPosition;
    public GameObject effectDamage;

	private void Start()
	{
		effectPosition = gameObject.GetComponent<RectTransform>();
	}

	private void OnMouseDown()
	{
		PlayDemageEffect();
	}

	protected void PlayDemageEffect()
	{
		Instantiate(effectDamage, effectPosition.position.normalized, Quaternion.identity);
	}
}
