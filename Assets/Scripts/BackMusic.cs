using UnityEngine;
using UnityEngine.SceneManagement;

public class BackMusic : MonoBehaviour
{
	[Header("Tags")]
	[SerializeField] private string createTag;
	private void Awake()
	{
		GameObject soundScene = GameObject.FindWithTag(this.createTag);
		if (soundScene != null)
		{
			Destroy(this.gameObject);
		}
		else
		{
			this.gameObject.tag = this.createTag;
			DontDestroyOnLoad(this.gameObject);
		}
	}

	private bool isStarted;
	private void Update()
	{
		if (SceneManager.GetActiveScene().buildIndex == 1) // Game Scene
		{
			gameObject.GetComponent<AudioSource>().Stop();
			isStarted = false;
		}
		else
		{
			if (!isStarted)
			{
				gameObject.GetComponent<AudioSource>().Play();
				isStarted = true;
			}
		}
	}
}
