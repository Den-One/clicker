using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoosterControllerKillStrong : AllGameObjects
{
	private bool isActivated;
	private int enemyBaseNumberOnGameSpace;

	private AudioSource sound;
	public AudioClip appearenceSound;
	public AudioClip audioClip;

	private SpriteRenderer boosterSprite;
	private Animator animator;
	private BoxCollider2D boxCollider;

	public Text pointsText;

	private void Start()
	{
		isActivated = false;
		enemyBaseNumberOnGameSpace = 3;
		sound = gameObject.GetComponent<AudioSource>();
		boosterSprite = gameObject.GetComponent<SpriteRenderer>();
		animator = gameObject.GetComponent<Animator>();
		boxCollider = gameObject.GetComponent<BoxCollider2D>();

		playAppearenceSound(); 
	}

	public void OnMouseDown()
	{
		strongEnemies = GameObject.FindGameObjectsWithTag("Strong Enemy");
		middleEnemies = GameObject.FindGameObjectsWithTag("Middle Enemy");
		weakEnemies = GameObject.FindGameObjectsWithTag("Weak Enemy");

		if (strongEnemies.Length > 1
			|| middleEnemies.Length > 1
			|| weakEnemies.Length > 1)
		{
			isActivated = true;
		}

		if (isActivated)
		{
			sound.PlayOneShot(audioClip);

			int points = strongEnemies.Length
				+ middleEnemies.Length
				+ weakEnemies.Length
				- enemyBaseNumberOnGameSpace;

			pointsText.text = SetNewValueAfterDeath(points);
			
			DestroyEnemies();
			RemoveBooster();
		}
	}

	protected string SetNewValueAfterDeath(int points)
	{
		int number = Convert.ToInt32(pointsText.text);
		number += points;
		string result = Convert.ToString(number);
		return result;
	}

	private RectTransform effectPosition;
	private SpriteRenderer enemyRenderer;
	private BoxCollider2D enemyCollider;
	public GameObject effectDamage;

	private void DestroyEnemies()
	{
		for (int i = strongEnemies.Length - 1; i > 0; --i)
		{
			effectPosition = strongEnemies[i].GetComponent<RectTransform>();
			enemyRenderer = strongEnemies[i].GetComponent<SpriteRenderer>();
			enemyCollider = strongEnemies[i].GetComponent<BoxCollider2D>();
			Instantiate(effectDamage, effectPosition.position.normalized, Quaternion.identity);
			
			Destroy(enemyRenderer);
			Destroy(enemyCollider);
			Destroy(strongEnemies[i], 0.3f);
		}

		for (int i = middleEnemies.Length - 1; i > 0; --i)
		{
			effectPosition = middleEnemies[i].GetComponent<RectTransform>();
			enemyRenderer = middleEnemies[i].GetComponent<SpriteRenderer>();
			enemyCollider = middleEnemies[i].GetComponent<BoxCollider2D>();
			Instantiate(effectDamage, effectPosition.position.normalized, Quaternion.identity);

			Destroy(enemyRenderer);
			Destroy(enemyCollider);
			Destroy(middleEnemies[i], 0.3f);
		}

		for (int i = weakEnemies.Length - 1; i > 0; --i)
		{
			Destroy(weakEnemies[i]);
		}
	}

	private void RemoveBooster()
	{
		Destroy(boosterSprite);
		Destroy(animator);
		Destroy(boxCollider);

		boosters2 = GameObject.FindGameObjectsWithTag("Booster 2");
		for (int i = boosters2.Length - 1; i > 0; --i)
		{
			Destroy(boosters2[i], 1.5f);
		}
	}

	private void playAppearenceSound()
	{
		if (GameObject.FindGameObjectsWithTag("Booster 2").Length > 1)
		{
			sound.PlayOneShot(appearenceSound);
		}
	}
}