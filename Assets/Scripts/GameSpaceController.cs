using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSpaceController : MenuController
{
	public Text pointsText;
	public void GoToMainMenuAndSaveResults()
	{
		string newRecord = pointsText.text;
		PlayerPrefs.SetString("playerRecord", newRecord);
		SceneManager.LoadScene(idMainMenu);
	}
}
